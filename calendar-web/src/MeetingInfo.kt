package ru.appmat

import ru.appmat.meeting.Meeting
import ru.appmat.meeting_attendee.MeetingAttendee
import ru.appmat.template.Template

class MeetingInfo (
    var meetingAttendee: MeetingAttendee,
    var meeting: Meeting,
    var template: Template
)