package ru.appmat

import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.client.*
import io.ktor.client.engine.apache.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.request.*
import io.ktor.features.*
import io.ktor.html.*
import io.ktor.http.*
import io.ktor.jackson.*
import io.ktor.locations.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.serialization.*
import io.ktor.sessions.*
import kotlinx.coroutines.runBlocking
import kotlinx.css.*
import kotlinx.datetime.Instant
import kotlinx.datetime.toJavaInstant
import kotlinx.datetime.toKotlinInstant
import kotlinx.html.*
import ru.appmat.meeting.Meeting
import ru.appmat.meeting_attendee.MeetingAttendee
import ru.appmat.slot.Slot
import ru.appmat.template.Template
import ru.appmat.user.User
import java.security.spec.KeySpec
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.ZoneOffset
import java.util.*
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.PBEKeySpec

/* Вопросы:
    1. Проверить модуль meeting_attendee, ибо там связи многие ко многим
 */

/*
    Что сделать:
    + 1. Создание шаблонов
    + 2. Изменение шаблонов
    + 3. Запись на мероприятие (дата и время любое)
    4. Запись на мероприятие только в доступное время, исключая отрезки, когда пользователь уже занят
    5. Выбор дат, в течение которых можно записать на шаблон мероприятия
    6. То, что раньше было написано в web, перенести в core
    7. Перенести логику проверки пользователя в core(!)
    8. По ссылке авторизация и редирект обратно
    + 9. Ссылка на слоты
    10. Выключение шаблона
    11. Изменение времени слотов
    12. Отменение всех встреч по шалону
    13. Удаление шаблона
 */

lateinit var coreAddress : String
lateinit var webPort : String
lateinit var webAddress : String

val coreRequestsSerializer = KotlinxSerializer()

/**
 *  path should start with "/"
 */
fun String.toCoreRequestLink(): String {
    return "${coreAddress}/api${this}"
}

fun String.toWebRequestLink(): String {
    return "${webAddress}${this}"
}

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

fun Application.module(testing: Boolean = false) {

    coreAddress = environment.config.config("core").property("address").getString()
    webPort = environment.config.config("ktor.deployment").property("port").getString()
    webAddress = environment.config.config("web").property("address").getString()


    install(Locations)

    install(ContentNegotiation) {
        json()
    }

    install(Sessions) {
        cookie<UserIdPrincipal>(
            Cookies.AUTH_COOKIE,
            storage = SessionStorageMemory()
        ) {
            cookie.path = "/"
            cookie.extensions["SameSite"] = "lax"
        }
    }

    install(Authentication) {
        //Configure Authentication with cookies
        session<UserIdPrincipal>(AuthName.SESSION) {
            challenge {
                call.respondRedirect("/login-error" + "?${Rooting.PREVIOUS_PAGE}=${call.request.uri.encodeURLParameter()}")
                // What to do if the user isn't authenticated
//                throw AuthenticationException()
            }
            validate { session: UserIdPrincipal ->
                // If you need to do additional validation on session data, you can do so here.
                session
            }

        }

        //Configure Authentication with login data
        form(AuthName.FORM) {
            userParamName = FormFields.EMAIL_AUTH
            passwordParamName = FormFields.PASSWORD_AUTH
            challenge {
                val params = call.receiveParameters()
                val previousPage = params[Rooting.PREVIOUS_PAGE]
                call.respondRedirect("/login-error" + "?${Rooting.PREVIOUS_PAGE}=$previousPage")
//                throw AuthenticationException()
            }
            validate { cred: UserPasswordCredential ->
                AuthProvider.tryAuth(cred.name, hashing(cred.password))
            }
        }
    }

    routing {
        install(StatusPages) {
            //Here you can specify responses on exceptions
            exception<AuthenticationException> {
                val params = call.receiveParameters()
                val previousPage = params[Rooting.PREVIOUS_PAGE]
                call.respondRedirect("/login-error" + "?${Rooting.PREVIOUS_PAGE}=$previousPage")
            }
            exception<AuthorizationException> {
                val params = call.receiveParameters()
                val previousPage = params[Rooting.PREVIOUS_PAGE]
                call.respondRedirect("/login-error" + "?${Rooting.PREVIOUS_PAGE}=$previousPage")
            }

            get("/health") {
                call.respond(HttpStatusCode.OK)
            }

            get("/ready") {
                call.respond(HttpStatusCode.OK)
            }
        }

        route("/login") {
            authenticate(AuthName.FORM) {
                post {
                    //Principal must not be null as we are authenticated

                    val principal = call.principal<UserIdPrincipal>()!!

                    val params = call.request.queryParameters
                    val previousPage: String? = params[Rooting.PREVIOUS_PAGE]

                    // Set the cookie to make session auth working
                    call.sessions.set(principal)

                    if (previousPage.isNullOrBlank())
                        call.respondRedirect("/user-info")
                    else
                        call.respondRedirect(previousPage)
                }
            }
        }

        route("/registration") {

            post {
                // getting email and password from form parameters
                val params = call.receiveParameters()
                val password = params[FormFields.PASSWORD_REGISTRATION]
                val email = params[FormFields.EMAIL_REGISTRATION]

                // Checking that data is valid
                if (email == null || password == null) {
                    call.respondHtml {
                        head {
                            title { +"Cannot get email or password" }
                        }
                        body {
                            label { +"Cannot get email or password" }
                        }
                    }
                    return@post
                }

                val newUser = User(
                    email = email,
                    password = hashing(password)
                )

                val client = HttpClient(Apache) {
                    install(JsonFeature) {
                        serializer = coreRequestsSerializer
                    }
                }

                val response = runBlocking {
                    client.post<HttpStatusCode> {
                        url("/user/registration".toCoreRequestLink())
                        body = coreRequestsSerializer.write(newUser)
                    }
                }

                if (response == HttpStatusCode.Accepted) {
                    call.respondHtml {
                        body {
                            label { +"User with this email or login already exist" }
                        }
                    }
                } else if (response == HttpStatusCode.Created){
                    call.respondRedirect("/")
                } else {
                    // shouldn't happen
                    call.respondHtml {
                        body {
                            label { +"Error in core module" }
                        }
                    }
                }
            }
        }


        authenticate(AuthName.SESSION) {
            get("/user-info") {
                //Principal must not be null as we are authenticated
                val principal = call.principal<UserIdPrincipal>()!!
                val client = HttpClient(Apache) {
                    install(JsonFeature) {
                        serializer = coreRequestsSerializer
                    }
                }
                val user = runBlocking {
                    client.get<User>("/user/${principal.name}".toCoreRequestLink())
                }

                call.respondHtml {
                    body {
                        showUserInfo(user)
                        div {
                            input {
                                type = InputType.button
                                value = "Изменить данные"
                                onClick = inputOnClickRedirect("/change-user-info")
                            }
                        }
                        form {
                            action = "/my-events"
                            method = FormMethod.get
                            button {
                                type = ButtonType.submit
                                +"""Мои мероприятия"""
                            }
                        }
                        div {
                            input {
                                type = InputType.button
                                value = "Мои встречи"
                                onClick = inputOnClickRedirect("/meetings")
                            }
                        }
                        form {
                            action = "/logout"
                            method = FormMethod.get
                            button {
                                type = ButtonType.submit
                                +"""Logout"""
                            }
                        }

                    }
                    head {
                        styleLink(url = "/styles.css")
                    }
                }
            }

            get("/change-user-info") {
                val principal = call.principal<UserIdPrincipal>()!!
                val client = HttpClient(Apache) {
                    install(JsonFeature) {
                        serializer = coreRequestsSerializer
                    }
                }
                val user = runBlocking {
                    client.get<User>("/user/${principal.name}".toCoreRequestLink())
                }
                call.respondHtml {
                    body {
                        form {
                            action = "user-change-post-request"
                            method = FormMethod.post
                            editUserInfo(user)
                            input {
                                type = InputType.submit
                                value = "Сохранить"
                            }
                        }

                    }
                    head {
                        script(type = ScriptType.textJavaScript) {
                            unsafe {
                                //language=JavaScript
                                raw("""
                                    (function() {
                                        var updateButton = document.getElementById('updateDetails');
                                        var cancelButton = document.getElementById('cancel');
                                        var favDialog = document.getElementById('favDialog');
                                    
                                        // Update button opens a modal dialog
                                        updateButton.addEventListener('click', function() {
                                          favDialog.showModal();
                                        });
                                    
                                        // Form cancel button closes the dialog box
                                        cancelButton.addEventListener('click', function() {
                                          favDialog.close();
                                        });
                                      })();
                                """.trimIndent())
                            }
                        }
                    }
                }
            }

            post("/user-change-post-request") {
                val principal = call.principal<UserIdPrincipal>()!!
                val params = call.receiveParameters()
                val user = getUserInfoFromParams(params, principal.name.toLong())
                val client = HttpClient(Apache) {
                    install(JsonFeature) {
                        serializer = coreRequestsSerializer
                    }
                }
                client.put<Unit> {
                    url("/user/${user.id}/update-info".toCoreRequestLink())
                    body = coreRequestsSerializer.write(user)
                }
                call.respondRedirect("/user-info")
            }

            get("/meetings") {
                val client = HttpClient(Apache) {
                    install(JsonFeature) {
                        serializer = coreRequestsSerializer
                    }
                }
                val principal = call.principal<UserIdPrincipal>()!!
                val meetingsAttendee = runBlocking {
                    client.get<List<MeetingAttendee>>(
                        "/meeting-attendee/user-meetings${principal.name.toLong()}".toCoreRequestLink()
                    )
                }
                val meetingsInfo: MutableList<MeetingInfo> = mutableListOf()
                for (index in meetingsAttendee.indices) {
                    val meeting = runBlocking {
                        client.get<Meeting>("/meeting/${meetingsAttendee[index].meetingId}".toCoreRequestLink())
                    }
                    val template = runBlocking {
                        client.get<Template>("/template/${meeting.template}".toCoreRequestLink())
                    }
                    meetingsInfo.add(
                        MeetingInfo(meetingsAttendee[index], meeting, template)
                    )
                }
                call.respondHtml {
                    body {
                        h2 { +"Мои мероприятия" }
                        if (meetingsInfo.isEmpty())
                            p { +"Нет назначенных мероприятий" }
                        for (meetingInfo in meetingsInfo) {
                            showMeeting(meetingInfo)
                        }
                    }
                }
            }

            get("/logout") {
                call.sessions.clear<UserIdPrincipal>()
                call.respondRedirect("/")
            }

            get("/my-events") {
                val client = HttpClient(Apache) {
                    install(JsonFeature) {
                        serializer = coreRequestsSerializer
                    }
                }

                val templates = runBlocking {
                    client.get<List<Template>>("/template".toCoreRequestLink())
                }

                val principal = call.principal<UserIdPrincipal>()!!
                val userTemplates = templates.filter { template -> template.creator == principal.name.toLong() }

                call.respondHtml {
                    body {
                        h2 { +"Мои шаблоны" }
                        div {
                            input {
                                type = InputType.button
                                onClick = inputOnClickRedirect("/create-new-event")
                                value = "Создать"
                            }
                        }
                        for (template in userTemplates) {
                            showTemplate(template)
                        }
                    }
                }
            }

            get("/create-new-event") {
                call.respondHtml {
                    body {
                        templateEditing("create-new-event-post", Template(), true)
                    }
                }
            }

            post("/create-new-event-post") {
                val client = HttpClient(Apache) {
                    install(JsonFeature) {
                        serializer = coreRequestsSerializer
                    }
                }

                val principal = call.principal<UserIdPrincipal>()!!
                val params = call.receiveParameters()
                val template = getTemplateFromParameters(params, 0, principal.name.toLong())
                val dateStart = LocalDate.parse(params[FormFields.SLOT_DATE_FROM])
                val dateEnd = LocalDate.parse(params[FormFields.SLOT_DATE_UNTIL])
                val timeStart = LocalTime.parse(params[FormFields.SLOT_TIME_FROM])
                val timeEnd = LocalTime.parse(params[FormFields.SLOT_TIME_UNTIL])

                val templateId = client.post<Long> {
                    url("/template".toCoreRequestLink())
                    body = coreRequestsSerializer.write(template)
                }
                var date = dateStart
                while (date <= dateEnd) {
                    val dateStartTime = LocalDateTime.of(date, timeStart)
                    val dateEndTime = LocalDateTime.of(date, timeEnd)
                    val slot = Slot(
                        0, dateStartTime.toInstant(ZoneOffset.UTC).toKotlinInstant(),
                        dateEndTime.toInstant(ZoneOffset.UTC).toKotlinInstant(), templateId
                    )
                    client.post<Unit> {
                        url("/slot".toCoreRequestLink())
                        body = coreRequestsSerializer.write(slot)
                    }
                    date = date.plusDays(1)
                }

                call.respondRedirect("/my-events")
            }

            get<TemplateChange> {
                val client = HttpClient(Apache) {
                    install(JsonFeature) {
                        serializer = coreRequestsSerializer
                    }
                }
                val template = runBlocking {
                    client.get<Template>("/template/${it.id}".toCoreRequestLink())
                }

                val principal = call.principal<UserIdPrincipal>()!!

                if (template.creator != principal.name.toLong()) {
                    call.respondText("Нет прав доступа")
                    return@get
                }

                call.respondHtml {
                    body {
                        templateEditing("template-change${template.id}", template, false)
                    }
                }
            }

            post<TemplateChange> {
                val params = call.receiveParameters()
                val principal = call.principal<UserIdPrincipal>()!!
                val template = getTemplateFromParameters(params, it.id, principal.name.toLong())
                val client = HttpClient(Apache) {
                    install(JsonFeature) {
                        serializer = coreRequestsSerializer
                    }
                }
                client.put<Unit> {
                    url("/template/${template.id}".toCoreRequestLink())
                    body = coreRequestsSerializer.write(template)
                }
                call.respondRedirect("/my-events")
            }

            post<TemplateDelete> {
                val client = HttpClient(Apache) {
                    install(JsonFeature) {
                        serializer = coreRequestsSerializer
                    }
                }
                client.delete<Unit>("/template/${it.id}".toCoreRequestLink())
                call.respondRedirect("/my-events")
            }

            get<Event> {
                val client = HttpClient(Apache) {
                    install(JsonFeature) {
                        serializer = coreRequestsSerializer
                    }
                }
                val template = runBlocking {
                    client.get<Template>("/template/${it.templateId}".toCoreRequestLink())
                }
                val slots = runBlocking {
                    client.get<List<Slot>>("/slot/of-template${it.templateId}".toCoreRequestLink())
                }

                call.respondHtml {
                    body {
                        if (!template.enabled) {
                            p { +"Это мероприятие больше не доступно для записи" }
                        }
                        else {
                            h3 { +template.title }
                            p { +template.summary }
                            if (slots.isEmpty()) {
                                p { +"Нет доступных слотов для записи" }
                            } else {
                                for (slot in slots) {
                                    val date = LocalDate.ofInstant(slot.startTime.toJavaInstant(), ZoneOffset.UTC)
                                    div {
                                        input {
                                            type = InputType.button
                                            value = "${date.dayOfMonth} ${date.month.name}"
                                            onClick = inputOnClickRedirect("/event${it.templateId}/time${slot.id}")
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            get<Event.Time> {
                val client = HttpClient(Apache) {
                    install(JsonFeature) {
                        serializer = coreRequestsSerializer
                    }
                }
                val template = runBlocking {
                    client.get<Template>("/template/${it.parent.templateId}".toCoreRequestLink())
                }
                val slot = runBlocking {
                    client.get<Slot>("/slot/${it.slotId}".toCoreRequestLink())
                }
                var dateTime = LocalDateTime.ofInstant(slot.startTime.toJavaInstant(), ZoneOffset.UTC)
                val dateTimeEnd = LocalDateTime.ofInstant(slot.endTime.toJavaInstant(), ZoneOffset.UTC)
                call.respondHtml {
                    body {
                        h3 { +template.title }
                        p { +template.summary }
                        p { +"${dateTime.dayOfMonth} ${dateTime.month.name}" }
                        while (dateTime < dateTimeEnd) {
                            val endTime = dateTime.plusMinutes(template.duration.toLong())
                            form {
                                method = FormMethod.post
                                action = "post-request-${dateTime.toEpochSecond(ZoneOffset.UTC)}"
                                input {
                                    value = "${dateTime.hour}:${dateTime.minute}-${endTime.hour}:${endTime.minute}"
                                    type = InputType.submit
                                }
                            }
                            dateTime = dateTime.plusMinutes(template.availability_increments.toLong())
                        }
                    }
                }
            }

            post<Event.PostRequest> {
                val client = HttpClient(Apache) {
                    install(JsonFeature) {
                        serializer = coreRequestsSerializer
                    }
                }
                val now = LocalDateTime.now().toInstant(ZoneOffset.UTC).toKotlinInstant()
                val meeting = Meeting(0, now, Instant.fromEpochSeconds(it.startTime), it.parent.templateId, now, "")
                val meetingId = client.post<Long> {
                    url("/meeting".toCoreRequestLink())
                    body = coreRequestsSerializer.write(meeting)
                }
                val template = runBlocking {
                    client.get<Template>("/template/${it.parent.templateId}".toCoreRequestLink())
                }
                val principal = call.principal<UserIdPrincipal>()!!
                // TODO: status and user availability
                if (principal.name.toLong() != template.creator) {
                    val meetingAttendeeCreator = MeetingAttendee(meetingId, template.creator, "", false)
                    client.post<Unit> {
                        url("/meeting-attendee".toCoreRequestLink())
                        body = coreRequestsSerializer.write(meetingAttendeeCreator)
                    }
                }
                val meetingAttendeeThisUser = MeetingAttendee(meetingId, principal.name.toLong(), "", false)
                client.post<Unit> {
                    url("/meeting-attendee".toCoreRequestLink())
                    body = coreRequestsSerializer.write(meetingAttendeeThisUser)
                }
                call.respondRedirect("/meetings")
            }

        }

        get("/") {

            val params = call.request.queryParameters
            val previousPage: String? = params[Rooting.PREVIOUS_PAGE]

            call.respondHtml {
                body {
                    form {
                        action = "/login?${Rooting.PREVIOUS_PAGE}=${previousPage?:""}".toWebRequestLink()
                        method = FormMethod.post
                        input {
                            type = InputType.email
                            name = FormFields.EMAIL_AUTH
                            placeholder = "Введите Ваш email"
                            required = true

                        }
                        input {
                            type = InputType.password
                            name = FormFields.PASSWORD_AUTH
                            placeholder = "Введите пароль"
                            required = true
                        }
                        button {
                            type = ButtonType.submit
                            name = FormFields.AUTH_BUTTON
                            +"""Войти"""
                        }
                    }

                    form {
                        action = "registration"
                        method = FormMethod.post
                        input {
                            type = InputType.email
                            name = FormFields.EMAIL_REGISTRATION
                            placeholder = "Введите Ваш email"
                            required = true
                        }
                        input {
                            type = InputType.password
                            name = FormFields.PASSWORD_REGISTRATION
                            placeholder = "Задайте пароль"
                            required = true
                        }
                        button {
                            type = ButtonType.submit
                            name = FormFields.REGISTRATION_BUTTON
                            +"""Зарегестрироваться"""
                        }
                    }
                }

            }
        }

        get("/login-error") {
            val params = call.request.queryParameters
            val previousPage: String? = params[Rooting.PREVIOUS_PAGE]
            call.respondHtml {
                body {
                    label { +"""Ошибка авторизации""" }
                    input {
                        onClick = inputOnClickRedirect("/"+ "?${Rooting.PREVIOUS_PAGE}=${previousPage?:""}")
                        type = InputType.button
                        value = """Вернуться"""
                    }
                }
            }
        }

        get("/styles.css") {
            call.respondCss {
                body {
                    backgroundColor = Color.darkGray
                }
                p {
                    fontSize = 4.em
                }
                rule("p.myclass") {
                    color = Color.white
                }
            }
        }

    }
}

suspend inline fun ApplicationCall.respondCss(builder: CSSBuilder.() -> Unit) {
    this.respondText(CSSBuilder().apply(builder).toString(), ContentType.Text.CSS)
}

fun hashing(string: String): String {
    val rnd = Random(string.hashCode().toLong())
    val salt = ByteArray(16)
    rnd.nextBytes(salt)
    val spec: KeySpec = PBEKeySpec(string.toCharArray(), salt, 65536, 128)
    val factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1")
    val hash = factory.generateSecret(spec).encoded
    return String(hash)
}

fun FlowContent.templateEditing(
    actionLink: String,
    template: Template,
    creating: Boolean
) = form {
    method = FormMethod.post
    action = actionLink
    div {
        label { +"Название мероприятия" }
        input {
            name = FormFields.TEMPLATE_TITLE
            type = InputType.text
            required = true
            value = template.title
        }
    }
    div {
        label { +"Описание мероприятия" }
        input {
            name = FormFields.TEMPLATE_SUMMARY
            type = InputType.text
            value = template.summary
        }
    }
    div {
        label { +"Длительность" }
        input {
            name = FormFields.TEMPLATE_DURATION
            required = true
            type = InputType.number
            value = (template.duration).toString()
        }
    }
    div {
        label { +"Интервал между доступным временем записи" }
        input {
            name = FormFields.TEMPLATE_AVAILABILITY_INCREMENTS
            required = true
            type = InputType.number
            value = (template.availability_increments).toString()
        }
    }
    div {
        label { +"Максимальное колчиество записей в день" }
        input {
            name = FormFields.TEMPLATE_MAX_PER_DAY
            type = InputType.number
            placeholder = "Неограниченно"
            value =
                if (template.max_per_day == -1)
                    ""
                else
                    template.max_per_day.toString()
        }
    }
    div {
        label { +"Максимальное колчиество участников на одном временном промежутке" }
        input {
            name = FormFields.TEMPLATE_MAX_INVITEES_IN_A_SPOT
            required = true
            type = InputType.number
            value = (template.max_invitees_in_a_spot).toString()
        }
    }
    div {
        label { +"Количество минут, которые будут заняты перед мероприятием" }
        input {
            name = FormFields.TEMPLATE_BUFFER_BEFORE
            required = true
            type = InputType.number
            value = (template.buffer_before).toString()
        }
    }
    div {
        label { +"Количество минут, которые будут заняты после мероприятия" }
        input {
            name = FormFields.TEMPLATE_BUFFER_AFTER
            required = true
            type = InputType.number
            value = (template.buffer_after).toString()
        }
    }
    if (creating) {
        div {
            label { +"Даты для записи" }
            label { +" от " }
            input {
                name = FormFields.SLOT_DATE_FROM
                type = InputType.date
                required = true
            }
            label { +" до " }
            input {
                name = FormFields.SLOT_DATE_UNTIL
                type = InputType.date
                required = true
            }
        }
        div {
            label { +"Временной отрезок для записи" }
            label { +" от " }
            input {
                name = FormFields.SLOT_TIME_FROM
                type = InputType.time
                required = true
            }
            label { +" до " }
            input {
                name = FormFields.SLOT_TIME_UNTIL
                type = InputType.time
                required = true
            }
        }
    }
    input {
        type = InputType.submit
        value = "Сохранить"
    }
}

fun FlowContent.showTemplate(
    template: Template
) = div {
    h3 {
        +template.title
    }
    p {
        +template.summary
    }
    div {
        label { +"Ссылка для записи на мероприятие" }
    }
    div {
        label { +"/event${template.id}".toWebRequestLink() }
    }
    input {
        type = InputType.button
        value = "Изменить"
        onClick = inputOnClickRedirect("/template-change${template.id}")
    }
    form {
        method = FormMethod.post
        action = "/template-delete${template.id}"
        input {
            type = InputType.submit
            value = "Удалить"
        }
    }
}

fun getTemplateFromParameters(params: Parameters, id: Long, creator: Long): Template {
    val maxPerDay =
        if (params[FormFields.TEMPLATE_MAX_PER_DAY] != null
            && params[FormFields.TEMPLATE_MAX_PER_DAY] != ""
        )
            params[FormFields.TEMPLATE_MAX_PER_DAY]!!.toInt()
        else
            -1
    return Template(
        id = id,
        creator = creator,
        title = params[FormFields.TEMPLATE_TITLE]!!,
        summary = params[FormFields.TEMPLATE_SUMMARY]!!,
        duration = params[FormFields.TEMPLATE_DURATION]!!.toInt(),
        availability_increments = params[FormFields.TEMPLATE_AVAILABILITY_INCREMENTS]!!.toInt(),
        max_per_day = maxPerDay,
        buffer_before = params[FormFields.TEMPLATE_BUFFER_BEFORE]!!.toInt(),
        buffer_after = params[FormFields.TEMPLATE_BUFFER_AFTER]!!.toInt(),
        invitation_link = "/events${id}".toWebRequestLink(),
        max_invitees_in_a_spot = params[FormFields.TEMPLATE_MAX_INVITEES_IN_A_SPOT]!!.toInt(),
        enabled = true
    )
}

fun inputOnClickRedirect(path: String): String {
    return "location.href = \"$path\""
}

fun FlowContent.showMeeting(
    meetingInfo: MeetingInfo
) =
    div {
        h3 {
            +meetingInfo.template.title
        }
        p {
            +meetingInfo.template.summary
        }
        // TODO: time zone problem
        val dateStart = LocalDateTime.ofInstant(meetingInfo.meeting.start_time.toJavaInstant(), ZoneOffset.UTC)
//    val dateStart = LocalDateTime.ofEpochSecond(.toEpochSeconds(), 0, ZoneOffset.UTC)
        p {
            +(dateStart.dayOfMonth.toString() + " " + dateStart.month.name)
        }
        val dateEnd = dateStart.plusMinutes(meetingInfo.template.duration.toLong())
        p {
            +("${dateStart.hour}:${dateStart.minute}-${dateEnd.hour}:${dateEnd.minute}")
        }
    }

fun FlowContent.showUserInfo(
    user: User
) =
    div {
        div {
            label { +("Имя:") }
            label { +(user.name) }
        }
        div {
            label { +("Фамилия:") }
            label { +(user.surname) }
        }
        div {
            label { +("Логин:") }
            label { +(user.login) }
        }
        div {
            label { +("Email:") }
            label { +(user.email) }
        }
    }

fun FlowContent.editUserInfo(
    user: User
) =
    div {
        div {
            label { +("Имя:") }
            input {
                name = FormFields.USER_NAME
                type = InputType.text
                value = (user.name)
            }
        }
        div {
            label { +("Фамилия:") }
            input {
                name = FormFields.USER_SURNAME
                type = InputType.text
                value = (user.surname)
            }
        }
        div {
            label { +("Логин:") }
            input {
                name = FormFields.USER_LOGIN
                type = InputType.text
                value = (user.login)
            }
        }
    }

fun getUserInfoFromParams(params: Parameters, id: Long): User {
    return User(
        id = id,
        name = params[FormFields.USER_NAME] ?: "",
        surname = params[FormFields.USER_SURNAME] ?: "",
        login = params[FormFields.USER_LOGIN] ?: "",
        email = "", password = "", photo = ""
    )
}
