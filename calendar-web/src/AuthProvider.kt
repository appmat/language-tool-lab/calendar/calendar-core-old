package ru.appmat

import io.ktor.auth.*
import io.ktor.client.*
import io.ktor.client.engine.apache.*
import io.ktor.client.features.json.*
import io.ktor.client.request.*
import kotlinx.coroutines.runBlocking
import ru.appmat.user.User

object AuthProvider {

    fun tryAuth(email: String, password: String): UserIdPrincipal? {

        val client = HttpClient(Apache) {
            install(JsonFeature) {
                serializer = coreRequestsSerializer
            }
        }

        val userToAuth = User(email = email, password = password)
        return try {
            val user: User = runBlocking {
                client.post<User> {
                    url("/user/auth".toCoreRequestLink())
                    body = coreRequestsSerializer.write(userToAuth)
                }
            }
            UserIdPrincipal(user.id.toString())
        } catch (e: Exception) {
            null
        }


        // if (user != null) {
        //     return UserIdPrincipal(user.id.toString())
        // }
    }
}