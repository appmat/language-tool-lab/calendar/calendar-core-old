package ru.appmat

object Cookies {
    const val AUTH_COOKIE = "auth"
}

object AuthName {
    const val SESSION = "auth_session"
    const val FORM = "auth_form"
    const val REGISTRATION = "registration_form"
}

object FormFields {
    const val EMAIL_AUTH = "auth_username_field"
    const val PASSWORD_AUTH = "auth_password_field"
    const val AUTH_BUTTON = "auth_input_button"

    const val PASSWORD_REGISTRATION = "registration_password_field"
    const val EMAIL_REGISTRATION = "registration_email_field"
    const val REGISTRATION_BUTTON = "registration_button"

    const val EVENT_BUTTON = "event_button"

    const val EVENT_DAY_TO_REGISTER = "EVENT_DAY_TO_REGISTER"
    const val EVENT_MONTH_TO_REGISTER = "EVENT_DAY_TO_REGISTER"

    const val TEMPLATE_TITLE = "TEMPLATE_TITLE"
    const val TEMPLATE_SUMMARY = "TEMPLATE_SUMMARY"
    const val TEMPLATE_DURATION = "TEMPLATE_DURATION"
    const val TEMPLATE_AVAILABILITY_INCREMENTS = "TEMPLATE_AVAILABILITY_INCREMENTS"
    const val TEMPLATE_MAX_PER_DAY = "TEMPLATE_MAX_PER_DAY"
    const val TEMPLATE_MAX_INVITEES_IN_A_SPOT = "TEMPLATE_MAX_INVITEES_IN_A_SPOT"
    const val TEMPLATE_BUFFER_BEFORE = "TEMPLATE_BUFFER_BEFORE"
    const val TEMPLATE_BUFFER_AFTER = "TEMPLATE_BUFFER_AFTER"

    const val USER_NAME = "USER_NAME"
    const val USER_SURNAME = "USER_SURNAME"
    const val USER_LOGIN = "USER_LOGIN"

    const val SLOT_DATE_FROM = "SLOT_DATE_FROM"
    const val SLOT_DATE_UNTIL = "SLOT_DATE_UNTIL"

    const val SLOT_TIME_FROM = "SLOT_TIME_FROM"
    const val SLOT_TIME_UNTIL = "SLOT_TIME_UNTIL"
}

object Rooting {
    const val PREVIOUS_PAGE = "PREVIOUS_PAGE"
}