package ru.appmat

import io.ktor.locations.*

@Location("/event{templateId}")
class Event(val templateId: Long)  {
    @Location("/time{slotId}")
    class Time(val slotId: Long, val parent: Event)

    @Location("/post-request-{startTime}")
    class PostRequest(val startTime: Long, val parent: Event)
}

@Location("/template-change{id}")
class TemplateChange(val id: Long)

@Location("/template-delete{id}")
class TemplateDelete(val id: Long)

@Location("/user-change{id}")
class UserChange(val id: Long)