CREATE TABLE USERS (
    id BIGSERIAL PRIMARY KEY,
    email TEXT,
    name VARCHAR(50),
    surname VARCHAR(50),
    login TEXT,
    password TEXT,
    photo TEXT DEFAULT NULL -- TODO: link to default photo
);

CREATE TABLE TEMPLATE (
    id BIGSERIAL PRIMARY KEY,
    creator BIGINT REFERENCES USERS ON UPDATE CASCADE,
    title VARCHAR(255),
    summary VARCHAR(3000),
    duration INT, -- in minutes
    availability_increments INT default 15, -- interval in minutes between available time to schedule
    max_per_day	INT default -1, -- maximum events per day for this template, -1 = unlimited
    buffer_before INT default 0, -- minutes before meeting that will be busy
    buffer_after INT default 0, -- minutes after meeting that will be busy
    invitation_link TEXT,
    max_invitees_in_a_spot INT DEFAULT 1, -- quantity of members that can schedule at one meeting at the same time
    enabled BOOLEAN DEFAULT true
);

CREATE TABLE SLOT (
    id BIGSERIAL PRIMARY KEY,
    start_time TIMESTAMP,
    end_time TIMESTAMP,
    template BIGINT REFERENCES TEMPLATE ON UPDATE CASCADE ON DELETE CASCADE
);


CREATE TABLE MEETING (
    id BIGSERIAL PRIMARY KEY,
    created_time TIMESTAMP,
    start_time TIMESTAMP,
    template BIGINT REFERENCES TEMPLATE ON UPDATE CASCADE,
    updated_time TIMESTAMP,
    status TEXT
);

CREATE TABLE MEETING_ATTENDEES (
    meeting BIGINT NOT NULL,
    user_id BIGINT NOT NULL,
    FOREIGN KEY (meeting) REFERENCES MEETING ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (user_id) REFERENCES USERS ON UPDATE CASCADE ON DELETE CASCADE,
    UNIQUE (meeting, user_id),
    user_status TEXT,
    user_availability BOOLEAN NOT NULL DEFAULT false -- TRUE - available, FALSE - busy
);