package ru.appmat.meeting

import org.jetbrains.exposed.dao.id.LongIdTable
import org.jetbrains.exposed.sql.`java-time`.timestamp

internal object MeetingTable : LongIdTable("Meeting")  {
    val created_time = timestamp("created_time")
    val start_time = timestamp("start_time")
    val template = long("template")
    val updated_time = timestamp("updated_time")
    val status = text("status")
}