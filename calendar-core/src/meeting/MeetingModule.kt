package ru.appmat.meeting

import io.ktor.application.*
import io.ktor.http.*
import io.ktor.locations.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import org.jetbrains.exposed.sql.transactions.transaction
import ru.appmat.API

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.meetingModule() {
    val meetingService = MeetingDAO(MeetingTable)
    routing {
        meeting(meetingService)
    }
}

/**
 * CRUD
 * Create
 * Read
 * Update
 * Delete
 */
internal fun Routing.meeting(meetingService: MeetingDAO) {
    get<API.Meetings> {
        val meetingList = transaction {
            meetingService.list()
        }
        call.respond(meetingList)
    }
    post<API.Meetings> {
        val meeting = call.receive<Meeting>()
        val id = transaction {
            meetingService.persistAndGetId(meeting)
        }
        call.respond(id)
    }
    get<API.Meetings.Meeting> {
        val meeting = transaction {
            meetingService.load(it.id)
        }
        call.respond(meeting)
    }
    put<API.Meetings.Meeting> {
        val meeting = call.receive<Meeting>()
        transaction {
            meetingService.update(meeting)
        }
        call.respond(HttpStatusCode.OK)
    }
    delete<API.Meetings.Meeting> {
        transaction {
            meetingService.delete(it.id)
        }
        call.respond(HttpStatusCode.OK)
    }


}