package ru.appmat.meeting

import kotlinx.datetime.toJavaInstant
import kotlinx.datetime.toKotlinInstant
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.statements.UpdateBuilder

internal class MeetingDAO(private val meetingTable: MeetingTable) {
    private fun ResultRow.toMeeting() = Meeting (
        id = this[MeetingTable.id].value,
        created_time = this[MeetingTable.created_time].toKotlinInstant(),
        start_time = this[MeetingTable.start_time].toKotlinInstant(),
        template = this[MeetingTable.template],
        updated_time = this[MeetingTable.updated_time].toKotlinInstant(),
        status = this[MeetingTable.status]
    )

    private fun UpdateBuilder<*>.fromMeeting(meeting: Meeting) {
        this[MeetingTable.created_time] = meeting.created_time.toJavaInstant()
        this[MeetingTable.start_time] = meeting.start_time.toJavaInstant()
        this[MeetingTable.template] = meeting.template
        this[MeetingTable.updated_time] = meeting.updated_time.toJavaInstant()
        this[MeetingTable.status] = meeting.status
    }

    fun load(id : Long) =
        meetingTable.select {
            MeetingTable.id eq id
        }.single().toMeeting()

    fun list() =
        meetingTable.selectAll().mapNotNull {
            it.toMeeting()
        }

    fun update(meeting : Meeting) =
        meetingTable.update ({
            MeetingTable.id eq meeting.id
        }) {
            it.fromMeeting(meeting)
        }

    fun persist(meeting : Meeting) =
        meetingTable.insert {
            it.fromMeeting(meeting)
        }

    fun delete(id : Long) =
        meetingTable.deleteWhere {
            MeetingTable.id eq id
        }

    fun persistAndGetId(meeting: Meeting) =
        meetingTable.insertAndGetId {
            it.fromMeeting(meeting)
        }.value
}