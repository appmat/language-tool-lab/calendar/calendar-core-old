package ru.appmat

import io.ktor.locations.*

@Location("/health")
class Health {

}

@Location("/ready")
class Ready {

}

@Location("/api")
class API {
    @Location("/user")
    class Users(val api: API) {
        @Location("/{id}")
        data class User(val id: Long, val parent: Users) {
            @Location("/update-info")
            data class UpdateInfo(val parent: User)

            @Location("/update-password")
            data class UpdatePassword(val parent: User)

            @Location("/update-email")
            data class UpdateEmail(val parent: User)
        }

        @Location("/public_info{id}")
        data class PublicInfo(val id: Long, val parent: Users)


        @Location("/auth")
        class Authentication(val parent: Users)

        @Location("/registration")
        class Registration(val parent: Users)
    }

    @Location("/template")
    class Templates(val api: API) {
        @Location("/{id}")
        data class Template(val id: Long, val parent: Templates)
    }

    @Location("/slot")
    class Slots(val api: API) {
        @Location("/{id}")
        data class Slot(val id: Long, val parent: Slots)

        @Location("/of-template{templateId}")
        data class OfTemplate(val templateId: Long, val parent: Slots)
    }

    @Location("/meeting")
    class Meetings(val api: API) {
        @Location("/{id}")
        data class Meeting(val id: Long, val parent: Meetings)
    }

    @Location("/meeting-attendee")
    class MeetingAttendees(val api: API) {

        @Location("/user-meetings{userId}")
        data class UserMeetings(val userId: Long, val parent: MeetingAttendees) {
            @Location("/{meetingId}")
            data class MeetingAttendee(val meetingId: Long, val parent: UserMeetings)
        }

        @Location("/meeting-users{meetingId}")
        data class MeetingUsers(val meetingId: Long, val parent: MeetingAttendees) {
            @Location("/{userId}")
            data class MeetingAttendee(val userId: Long, val parent: MeetingUsers)
        }
    }

}
