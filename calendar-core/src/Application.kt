package ru.appmat

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import io.ktor.application.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.locations.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.serialization.*
import org.flywaydb.core.Flyway
import org.jetbrains.exposed.sql.Database

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {
    install(Locations)
    install(ContentNegotiation) {
        json()
    }
    val hikariDataSource = HikariDataSource(
        HikariConfig().apply {
            val hikariAppConfig = environment.config.config("ktor.hikari")
            username = hikariAppConfig.property("user").getString()
            password = hikariAppConfig.property("password").getString()
            jdbcUrl = hikariAppConfig.property("jdbcUrl").getString()
        }
    )

    routing {
        get<Health> {
            call.respond(HttpStatusCode.OK)
        }

        get<Ready> {
            call.respond(HttpStatusCode.OK)
        }
    }

    Flyway.configure().dataSource(hikariDataSource).load().migrate()
    Database.connect(hikariDataSource)

}



