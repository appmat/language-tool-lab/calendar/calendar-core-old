package ru.appmat.user

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.statements.UpdateBuilder

/**
 * DAO -- Data Access Object
 */
internal class UserDAO(private val userTable: UserTable) {
    private fun ResultRow.toUser() = User (
        id = this[UserTable.id].value,
        email = this[UserTable.email],
        name = this[UserTable.name],
        surname = this[UserTable.surname],
        login = this[UserTable.login],
        password = this[UserTable.password],
        photo = this[UserTable.photo],
    )

    private fun UpdateBuilder<*>.fromUser(user: User) {
        this[UserTable.email] = user.email
        this[UserTable.name] = user.name
        this[UserTable.surname] = user.surname
        this[UserTable.login] = user.login
        this[UserTable.password] = user.password
        this[UserTable.photo] = user.photo
    }

    fun load(id : Long) =
        userTable.select {
            UserTable.id eq id
        }.single().toUser()

    fun list() =
        userTable.selectAll().mapNotNull {
            it.toUser()
        }

    fun update(user : User) =
        userTable.update ({
            UserTable.id eq user.id
        }) {
            it.fromUser(user)
        }

    fun persist(user : User) =
        userTable.insert {
            it.fromUser(user)
        }

    fun delete(id : Long) =
        userTable.deleteWhere {
            UserTable.id eq id
        }

}