package ru.appmat.user

import io.ktor.application.*
import io.ktor.http.*
import io.ktor.http.HttpStatusCode.Companion.Created
import io.ktor.http.HttpStatusCode.Companion.OK
import io.ktor.locations.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import org.jetbrains.exposed.sql.transactions.transaction
import ru.appmat.API

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.userModule() {
    val userService = UserDAO(UserTable)
    routing {
        user(userService)
    }
}

/**
 * CRUD
 * Create
 * Read
 * Update
 * Delete
 */
internal fun Routing.user(userService: UserDAO) {
    get<API.Users> {
        val userList = transaction {
            userService.list()
        }
        call.respond(userList)
    }

    post<API.Users> {
        val user = call.receive<User>()
        transaction {
            userService.persist(user)
        }
        call.respond(Created)
    }

    get<API.Users.User> {
        val user = transaction {
            userService.load(it.id)
        }

        // return user without password
        val userWithoutPassword = user.copy(password = "")
        call.
        respond(userWithoutPassword)
    }

    put<API.Users.User.UpdateInfo> {
        val userDB = transaction {
            userService.load(it.parent.id)
        }
        val user = call.receive<User>()
        val newUser = userDB.copy(login = user.login, name = user.name, surname = user.surname)
        transaction {
            userService.update(newUser)
        }
        call.respond(OK)
    }

    delete<API.Users.User> {
        transaction {
            userService.delete(it.id)
        }
        call.respond(OK)
    }

    post<API.Users.Authentication> {
        val userAuth = call.receive<User>()
        val userList = transaction {
            userService.list()
        }
        val userDB = userList.singleOrNull() { user -> user.email == userAuth.email}
        if (userDB != null && userDB.password == userAuth.password) {
            val userWithoutPassword = userDB.copy(password = "")
            call.respond(userWithoutPassword)
        }
        else
            call.respond(HttpStatusCode.Unauthorized)
    }

    get<API.Users.PublicInfo> {
        val user = transaction {
            userService.load(it.id)
        }
        val userPublicInfo = User(user.name, user.surname, user.login, user.photo)
        call.respond(userPublicInfo)
    }

    post<API.Users.Registration> {
        val newUser = call.receive<User>()
        val userList = transaction {
            userService.list()
        }
        val userDB = userList.find { user -> user.email == newUser.email }
        if (userDB != null) {
            call.respond(HttpStatusCode.Accepted)
        } else {
            transaction {
                userService.persist(newUser)
            }
            call.respond(Created)
        }
    }

}

