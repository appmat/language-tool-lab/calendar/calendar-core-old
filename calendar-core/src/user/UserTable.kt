package ru.appmat.user

import org.jetbrains.exposed.dao.id.LongIdTable
import org.jetbrains.exposed.sql.Column

internal object UserTable : LongIdTable("users") {
    val email : Column<String> = text("email")
    val name : Column<String> = varchar("name", 50)
    val surname : Column<String> = varchar("surname", 50)
    val login : Column<String> = text("login")
    val password : Column<String> = text("password")
    val photo : Column<String> = text("photo")
}