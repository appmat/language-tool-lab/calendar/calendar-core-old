package ru.appmat.slot

import org.jetbrains.exposed.dao.id.LongIdTable
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.`java-time`.timestamp
import java.time.Instant

internal object SlotTable : LongIdTable("slot")  {
    val startTime : Column<Instant> = timestamp("start_time")
    val endTime : Column<Instant> = timestamp("end_time")
    val template : Column<Long> = long("template")
}
