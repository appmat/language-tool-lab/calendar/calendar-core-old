package ru.appmat.slot

import io.ktor.application.*
import io.ktor.http.*
import io.ktor.locations.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import org.jetbrains.exposed.sql.transactions.transaction
import ru.appmat.API

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.slotModule() {
    val slotService = SlotDAO(SlotTable)
    routing {
        slot(slotService)
    }
}

/**
 * CRUD
 * Create
 * Read
 * Update
 * Delete
 */
internal fun Routing.slot(slotService: SlotDAO) {
    get<API.Slots> {
        val slotList = transaction {
            slotService.list()
        }
        call.respond(slotList)
    }
    post<API.Slots> {
        val slot = call.receive<Slot>()
        transaction {
            slotService.persist(slot)
        }
        call.respond(HttpStatusCode.Created)
    }
    get<API.Slots.Slot> {
        val slot = transaction {
            slotService.load(it.id)
        }
        call.respond(slot)
    }
    put<API.Slots.Slot> {
        val slot = call.receive<Slot>()
        transaction {
            slotService.update(slot)
        }
        call.respond(HttpStatusCode.OK)
    }
    delete<API.Slots.Slot> {
        transaction {
            slotService.delete(it.id)
        }
        call.respond(HttpStatusCode.OK)
    }

    get<API.Slots.OfTemplate> {
        val slotsOfTemplateList = transaction {
            slotService.ofTemplate(it.templateId)
        }
        call.respond(slotsOfTemplateList)
    }
}