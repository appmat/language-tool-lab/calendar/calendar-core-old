package ru.appmat.slot

import kotlinx.datetime.toJavaInstant
import kotlinx.datetime.toKotlinInstant
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.statements.UpdateBuilder

internal class SlotDAO(private val slotTable: SlotTable) {
    private fun ResultRow.toSlot() = Slot (
        id = this[SlotTable.id].value,
        template  = this[SlotTable.template],
        startTime = this[SlotTable.startTime].toKotlinInstant(),
        endTime   = this[SlotTable.endTime].toKotlinInstant(),
    )

    private fun UpdateBuilder<*>.fromSlot(slot: Slot) {
        this[SlotTable.template] = slot.template
        this[SlotTable.startTime] = slot.startTime.toJavaInstant()
        this[SlotTable.endTime] = slot.endTime.toJavaInstant()
    }

    fun load(id : Long) =
        slotTable.select {
            SlotTable.id eq id
        }.single().toSlot()

    fun list() =
        slotTable.selectAll().mapNotNull {
            it.toSlot()
        }

    fun update(slot : Slot) =
        slotTable.update ({
            SlotTable.id eq slot.id
        }) {
            it.fromSlot(slot)
        }

    fun persist(slot : Slot) =
        slotTable.insert {
            it.fromSlot(slot)
        }

    fun delete(id : Long) =
        slotTable.deleteWhere {
            SlotTable.id eq id
        }

    fun ofTemplate(templateId: Long) =
        slotTable.select {
            SlotTable.template eq templateId
        }.mapNotNull {
            it.toSlot()
        }
}