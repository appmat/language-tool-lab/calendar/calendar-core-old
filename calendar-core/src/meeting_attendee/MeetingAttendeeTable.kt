package ru.appmat.meeting_attendee

import org.jetbrains.exposed.sql.Table

internal object MeetingAttendeeTable : Table("meeting_attendees") {
    val meeting_id = long("meeting")
    val user_id = long("user_id")
    val user_status = text("user_status")
    val user_availability = bool("user_availability")
    override val primaryKey = PrimaryKey(meeting_id, user_id)
}