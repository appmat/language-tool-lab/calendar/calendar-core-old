package ru.appmat.meeting_attendee

import io.ktor.application.*
import io.ktor.http.*
import io.ktor.locations.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import org.jetbrains.exposed.sql.transactions.transaction
import ru.appmat.API

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.meetingAttendeeModule() {
    val meetingAttendeeService = MeetingAttendeeDAO(MeetingAttendeeTable)
    routing {
        meetingAttendee(meetingAttendeeService)
    }
}

/**
 * CRUD
 * Create
 * Read
 * Update
 * Delete
 */
internal fun Routing.meetingAttendee(meetingAttendeeService: MeetingAttendeeDAO) {
    get<API.MeetingAttendees> {
        val meetingAttendeeList = transaction {
            meetingAttendeeService.list()
        }
        call.respond(meetingAttendeeList)
    }
    post<API.MeetingAttendees> {
        val meetingAttendee = call.receive<MeetingAttendee>()
        transaction {
            meetingAttendeeService.persist(meetingAttendee)
        }
        call.respond(HttpStatusCode.Created)
    }


    get<API.MeetingAttendees.UserMeetings.MeetingAttendee> {
        val meetingAttendee = transaction {
            meetingAttendeeService.load(meetingId = it.meetingId, userId = it.parent.userId)
        }
        call.respond(meetingAttendee)
    }
    put<API.MeetingAttendees.UserMeetings.MeetingAttendee> {
        val meetingAttendee = call.receive<MeetingAttendee>()
        transaction {
            meetingAttendeeService.update(meetingAttendee)
        }
        call.respond(HttpStatusCode.OK)
    }
    delete<API.MeetingAttendees.UserMeetings.MeetingAttendee> {
        transaction {
            meetingAttendeeService.delete(meetingId = it.meetingId, userId = it.parent.userId)
        }
        call.respond(HttpStatusCode.OK)
    }


    get<API.MeetingAttendees.MeetingUsers.MeetingAttendee> {
        val meetingAttendee = transaction {
            meetingAttendeeService.load(meetingId = it.parent.meetingId, userId = it.userId)
        }
        call.respond(meetingAttendee)
    }
    put<API.MeetingAttendees.MeetingUsers.MeetingAttendee> {
        val meetingAttendee = call.receive<MeetingAttendee>()
        transaction {
            meetingAttendeeService.update(meetingAttendee)
        }
        call.respond(HttpStatusCode.OK)
    }
    delete<API.MeetingAttendees.MeetingUsers.MeetingAttendee> {
        transaction {
            meetingAttendeeService.delete(meetingId = it.parent.meetingId, userId = it.userId)
        }
        call.respond(HttpStatusCode.OK)
    }


    get<API.MeetingAttendees.UserMeetings> {
        val userMeetingsList = transaction {
            meetingAttendeeService.userMeetings(it.userId)
        }
        call.respond(userMeetingsList)
    }

    get<API.MeetingAttendees.MeetingUsers> {
        val meetingUsersList = transaction {
            meetingAttendeeService.meetingUsers(it.meetingId)
        }
        call.respond(meetingUsersList)
    }

}