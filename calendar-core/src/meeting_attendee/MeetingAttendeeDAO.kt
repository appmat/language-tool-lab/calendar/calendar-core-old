package ru.appmat.meeting_attendee

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.statements.UpdateBuilder

internal class MeetingAttendeeDAO(private val meetingAttendeeTable: MeetingAttendeeTable) {
    private fun ResultRow.toMeetingAttendee() = MeetingAttendee(
        meetingId = this[MeetingAttendeeTable.meeting_id],
        userId = this[MeetingAttendeeTable.user_id],
        userStatus = this[MeetingAttendeeTable.user_status],
        userAvailability = this[MeetingAttendeeTable.user_availability],
    )

    private fun UpdateBuilder<*>.fromMeetingAttendees(meetingAttendee: MeetingAttendee) {
        this[MeetingAttendeeTable.meeting_id] = meetingAttendee.meetingId
        this[MeetingAttendeeTable.user_id] = meetingAttendee.userId
        this[MeetingAttendeeTable.user_status] = meetingAttendee.userStatus
        this[MeetingAttendeeTable.user_availability] = meetingAttendee.userAvailability
    }

    fun load(meetingId: Long, userId: Long) =
        meetingAttendeeTable.select {
            (MeetingAttendeeTable.meeting_id eq meetingId) and (MeetingAttendeeTable.user_id eq userId)
        }.single().toMeetingAttendee()

    fun list() =
        meetingAttendeeTable.selectAll().mapNotNull {
            it.toMeetingAttendee()
        }

    fun update(meetingAttendees: MeetingAttendee) =
        meetingAttendeeTable.update({
            (MeetingAttendeeTable.meeting_id eq meetingAttendees.meetingId) and (MeetingAttendeeTable.user_id eq meetingAttendees.userId)
        }) {
            it.fromMeetingAttendees(meetingAttendees)
        }

    fun persist(meetingAttendees: MeetingAttendee) =
        meetingAttendeeTable.insert {
            it.fromMeetingAttendees(meetingAttendees)
        }

    fun delete(meetingId: Long, userId: Long) =
        meetingAttendeeTable.deleteWhere {
            (MeetingAttendeeTable.meeting_id eq meetingId) and (MeetingAttendeeTable.user_id eq userId)
        }

    fun meetingUsers(meetingId: Long) =
        meetingAttendeeTable.select {
            MeetingAttendeeTable.meeting_id eq meetingId
        }.mapNotNull {
            it.toMeetingAttendee()
        }

    fun userMeetings(userId: Long) =
        meetingAttendeeTable.select {
            MeetingAttendeeTable.user_id eq userId
        }.mapNotNull {
            it.toMeetingAttendee()
        }
}