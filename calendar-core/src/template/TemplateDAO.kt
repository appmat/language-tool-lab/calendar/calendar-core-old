package ru.appmat.template

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.statements.UpdateBuilder

internal class TemplateDAO(private val templateTable: TemplateTable) {
    private fun ResultRow.toTemplate() = Template (
        id = this[TemplateTable.id].value,
        creator = this[TemplateTable.creator],
        title = this[TemplateTable.title],
        summary = this[TemplateTable.summary],
        duration = this[TemplateTable.duration],
        availability_increments = this[TemplateTable.availability_increments],
        max_per_day = this[TemplateTable.max_per_day],
        buffer_before = this[TemplateTable.buffer_before],
        buffer_after = this[TemplateTable.buffer_after],
        invitation_link = this[TemplateTable.invitation_link],
        max_invitees_in_a_spot = this[TemplateTable.max_invitees_in_a_spot],
        enabled = this[TemplateTable.enabled],
    )

    private fun UpdateBuilder<*>.fromTemplate(template: Template) {
        this[TemplateTable.creator] = template.creator
        this[TemplateTable.title] = template.title
        this[TemplateTable.summary] = template.summary
        this[TemplateTable.duration] = template.duration
        this[TemplateTable.availability_increments] = template.availability_increments
        this[TemplateTable.max_per_day] = template.max_per_day
        this[TemplateTable.buffer_before] = template.buffer_before
        this[TemplateTable.buffer_after] = template.buffer_after
        this[TemplateTable.invitation_link] = template.invitation_link
        this[TemplateTable.max_invitees_in_a_spot] = template.max_invitees_in_a_spot
        this[TemplateTable.enabled] = template.enabled
    }

    fun load(id : Long) =
        templateTable.select {
            TemplateTable.id eq id
        }.single().toTemplate()

    fun list() =
        templateTable.selectAll().mapNotNull {
            it.toTemplate()
        }

    fun update(template : Template) =
        templateTable.update ({
            TemplateTable.id eq template.id
        }) {
            it.fromTemplate(template)
        }

    fun persist(template : Template) =
        templateTable.insert {
            it.fromTemplate(template)
        }

    fun delete(id : Long) =
        templateTable.deleteWhere {
            TemplateTable.id eq id
        }

    fun persistAndGetId(template : Template) =
        templateTable.insertAndGetId {
            it.fromTemplate(template)
        }.value
}