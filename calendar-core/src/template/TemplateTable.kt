package ru.appmat.template

import org.jetbrains.exposed.dao.id.LongIdTable
import org.jetbrains.exposed.sql.Column

internal object TemplateTable : LongIdTable("template") {
    val creator : Column<Long> = long("creator")
    val title : Column<String> = varchar("title", 255)
    val summary : Column<String> = varchar("summary", 3000)
    val duration : Column<Int> = integer("duration") // in minutes
    val availability_increments : Column<Int> = integer("availability_increments").default(15) // interval in minutes between available time to schedule
    val max_per_day: Column<Int> = integer("max_per_day").default(-1) // maximum events per day for this template, -1 = unlimited
    val buffer_before : Column<Int> =integer("buffer_before").default( 0) // minutes before meeting that will be busy
    val buffer_after : Column<Int> = integer("buffer_after").default(0) // minutes after meeting that will be busy
    val invitation_link : Column<String> = text("invitation_link")
    val max_invitees_in_a_spot : Column<Int> = integer("max_invitees_in_a_spot").default(1) // quantity of members that can schedule at one meeting at the same time
    val enabled : Column<Boolean> = bool("enabled").default(true)
}