package ru.appmat.template

import io.ktor.application.*
import io.ktor.http.*
import io.ktor.locations.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import org.jetbrains.exposed.sql.transactions.transaction
import ru.appmat.API

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.templateModule() {
    val templateService = TemplateDAO(TemplateTable)
    routing {
        template(templateService)
    }
}

/**
 * CRUD
 * Create
 * Read
 * Update
 * Delete
 */
internal fun Routing.template(templateService: TemplateDAO) {
    get<API.Templates> {
        val templateList = transaction {
            templateService.list()
        }
        call.respond(templateList)
    }
    post<API.Templates> {
        val template = call.receive<Template>()
        val templateId = transaction {
            templateService.persistAndGetId(template)
        }
        call.respond(templateId)
    }
    get<API.Templates.Template> {
        val template = transaction {
            templateService.load(it.id)
        }
        call.respond(template)
    }
    put<API.Templates.Template> {
        val template = call.receive<Template>()
        transaction {
            templateService.update(template)
        }
        call.respond(HttpStatusCode.OK)
    }
    delete<API.Templates.Template> {
        transaction {
            templateService.delete(it.id)
        }
        call.respond(HttpStatusCode.OK)
    }


}