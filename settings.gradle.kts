rootProject.name = "calendar-storage"
include("calendar-domain")
include("calendar-core")
include("calendar-web")