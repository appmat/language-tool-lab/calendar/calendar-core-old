package ru.appmat.template

import kotlinx.serialization.Serializable

@Serializable
data class Template (
    val id : Long,
    val creator : Long,
    val title : String,
    val summary : String,
    val duration : Int, // in minutes
    val availability_increments : Int = 15, // interval in minutes between available time to schedule
    val max_per_day: Int = -1, // maximum events per day for this template, -1 = unlimited
    val buffer_before : Int = 0, // minutes before meeting that will be busy
    val buffer_after : Int = 0, // minutes after meeting that will be busy
    val invitation_link : String,
    val max_invitees_in_a_spot : Int = 1, // quantity of members that can schedule at one meeting at the same time
    val enabled : Boolean = true
) {
    constructor() : this(0, 0, "", "", 30, 15, -1, 0, 0, "", 1, true)
}
