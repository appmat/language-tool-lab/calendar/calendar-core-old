package ru.appmat.meeting

import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable
import ru.appmat.InstantSerializer

@Serializable
data class Meeting (
    val id : Long,

    @Serializable(with = InstantSerializer::class)
    val created_time : Instant,

    @Serializable(with = InstantSerializer::class)
    val start_time : Instant,

    val template : Long,

    @Serializable(with = InstantSerializer::class)
    val updated_time : Instant,

    val status : String // cancelled, deleted, rescheduled, default

)