package ru.appmat.slot

import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable
import ru.appmat.InstantSerializer

@Serializable
data class Slot(
    val id: Long,

    @Serializable(with = InstantSerializer::class)
    val startTime: Instant,

    @Serializable(with = InstantSerializer::class)
    val endTime: Instant,

    val template: Long,
)

