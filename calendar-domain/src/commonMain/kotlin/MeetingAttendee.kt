package ru.appmat.meeting_attendee

import kotlinx.serialization.Serializable

@Serializable
data class MeetingAttendee (
    val meetingId : Long,
    val userId: Long,
    val userStatus: String, // yes, no, maybe
    val userAvailability: Boolean // TRUE - available, FALSE - busy
)