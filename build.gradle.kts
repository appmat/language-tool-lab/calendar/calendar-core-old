val ktor_version: String by project
val kotlin_version: String by project
val logback_version: String by project
val exposed_version: String by project
val flyway_version: String by project
val postgresql_version: String by project
val hikari_version: String by project

plugins {
}

group = "ru.appmat"
version = "0.0.1"

repositories {
    mavenLocal()
    jcenter()
    maven(url = "https://kotlin.bintray.com/kotlinx/") // soon will be just jcenter()
}

dependencies {

}
